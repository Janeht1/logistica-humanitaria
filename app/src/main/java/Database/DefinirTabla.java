package Database;

import android.provider.BaseColumns;

public final class DefinirTabla {

    public static abstract class Estado implements BaseColumns
    {
        public static final String TABLE_NAME = "Estados";
        public static final String COLUMN_NAME_ESTADO = "nombre";
    }
}
