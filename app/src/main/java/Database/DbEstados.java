package Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class DbEstados {

    Context context;
    EstadoAdapter myDbHelper;
    SQLiteDatabase db;
    String[] columnas = new String[]{
            DefinirTabla.Estado._ID,
            DefinirTabla.Estado.COLUMN_NAME_ESTADO
    };

    public DbEstados(Context context){
        this.context = context;
        this.myDbHelper = new EstadoAdapter(this.context);
    }

    public void openDatabase(){
        db = myDbHelper.getWritableDatabase();
    }

    public long insertContacto(Estados c){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estado.COLUMN_NAME_ESTADO, c.getNombreEstado());
        return db.insert(DefinirTabla.Estado.TABLE_NAME, null, values);
    }

    public long updateContacto(Estados c,int id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estado.COLUMN_NAME_ESTADO, c.getNombreEstado());
        Log.d("values", values.toString());
        return db.update(DefinirTabla.Estado.TABLE_NAME , values,
                DefinirTabla.Estado._ID + " = " + id,null);
    }

    public int deleteContacto(long id){
        return db.delete(DefinirTabla.Estado.TABLE_NAME,DefinirTabla.Estado._ID + "=?",
                new String[]{ String.valueOf(id) });
    }

    private Estados readContacto(Cursor cursor){
        Estados c = new Estados();
        c.setId(cursor.getInt(0));
        c.setNombreEstado(cursor.getString(1));
        return c;
    }

    public void close(){
        myDbHelper.close();
    }

    public Estados getContacto(long id){
        SQLiteDatabase db = myDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Estado.TABLE_NAME,
                columnas,
                DefinirTabla.Estado._ID + " = ?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null
        );
        c.moveToFirst();
        Estados contacto = readContacto(c);
        c.close();
        return contacto;
    }

    public ArrayList<Estados> allContactos(){
        Cursor cursor = db.query(DefinirTabla.Estado.TABLE_NAME,
                columnas, null, null, null, null, null);
        ArrayList<Estados> contactos = new ArrayList<Estados>();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Estados c = readContacto(cursor);
            contactos.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return contactos;
    }
}
