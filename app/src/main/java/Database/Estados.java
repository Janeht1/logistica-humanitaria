package Database;
import java.io.Serializable;

public class Estados implements Serializable{
    private int id;
    private String nombreEstado;
    private String idMovil;
    public Estados(){
        this.id = 0;
        this.nombreEstado = "";
        this.idMovil = "";
    }
    public Estados(int id, String nombreEstado, String idMovil){
        this.id=id;
        this.nombreEstado=nombreEstado;
        this.idMovil=idMovil;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }

    public String getIdMovil() {
        return idMovil;
    }

    public void setIdMovil(String idMovil) { this.idMovil = idMovil;
    }

}